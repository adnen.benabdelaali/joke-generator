/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pizza.jokegenerator.web.rest.vm;
